﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class FlashController : MonoBehaviour {

    public static bool IsFlashOn = false;

	public void ToggleFlash()
    {
        IsFlashOn = !IsFlashOn; 
        CameraDevice.Instance.SetFlashTorchMode(IsFlashOn);
    }
}
