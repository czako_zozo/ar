﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class MenuController: MonoBehaviour
    {
        public static MenuController Instance;

        private const string GameSceneName = "Game_Scene";
        private const string HighScoreScene = "HighScore";

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public void StartGame()
        {
            SceneManager.LoadScene(GameSceneName);
        }

        public void HighScoreClicked()
        {
            SceneManager.LoadScene(HighScoreScene);
        }

        public void ExitClicked()
        {
            Application.Quit();
        }
    }
}
