﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class FighterController : MonoBehaviour {

        public Transform EnemyTarget;
        public int Health = 100;
        public Slider PlayerHealthBar;
        public BoxCollider[] Colliders;
        public AudioClip[] AudiClips;
        public int PlayerLifeCount = 3;

        public static Animator PlayerAnimator;
        public static FighterController Instance;


        public static bool MoveBack = false;
        public static bool MoveForward = false;
        public static bool IsAttacking = false;

        private const string MoveBackTriggerName = "back";
        private const string MoveForwardTriggerName = "forward";
        private const string IdleTriggerName = "idle";
        private const string PunchTriggerName = "punch";
        private const string KickTriggerName = "kick";
        private const string ReactTriggerName = "react";
        private const string DeathTriggerName = "death";

        private const string FightIdleAnimatorStateName = "fight_idle";

        private const string PlayerTagName = "PlayerAvatar";

        private const int PlayerAvatarCloneIndex = 0;

        private int _damage = 1;
        private Vector3 _direction;
        private AudioSource _audio;
        private Vector3 _initialPlayerPosition;

        private readonly float RotationSpeed = 0.5f;

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        // Use this for initialization
        void Start ()
        {
            PlayerAnimator = GetComponent<Animator>();
            SetAllBoxColliders(false);
            _audio = GetComponent<AudioSource>();
            _initialPlayerPosition = transform.position;
            Debug.Log("Player initial position (x, y, z): (" + 
                _initialPlayerPosition.x + ", " + 
                _initialPlayerPosition.y + ", " + 
                _initialPlayerPosition.z + ")"); 
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (UDTEventHandler.IsTracking && GetYPosition() > 0)
            {
                ResetYPosition();
            }

            if (PlayerAnimator.GetCurrentAnimatorStateInfo(0).IsName(FightIdleAnimatorStateName))
            {
                _direction = EnemyTarget.position - this.transform.position;
                _direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(
                    this.transform.rotation, Quaternion.LookRotation(_direction),
                    RotationSpeed);
            }

            if (PlayerAnimator.GetCurrentAnimatorStateInfo(0).IsName(FightIdleAnimatorStateName))
            {
                IsAttacking = false;
                SetAllBoxColliders(false);
            }

            //if (GameController.AllowedMovement)
            //{
                if (!IsAttacking)
                {
                    if (MoveBack)
                    {
                        PlayerAnimator.SetTrigger(MoveBackTriggerName);
                        PlayerAnimator.ResetTrigger(IdleTriggerName);
                        SetAllBoxColliders(false);
                    }
                    else
                    {
                        PlayerAnimator.SetTrigger(IdleTriggerName);
                        PlayerAnimator.ResetTrigger(MoveBackTriggerName);
                    }

                    if (MoveForward)
                    {
                        PlayerAnimator.SetTrigger(MoveForwardTriggerName);
                        PlayerAnimator.ResetTrigger(IdleTriggerName);
                        SetAllBoxColliders(false);
                    }
                    else if (!MoveBack)
                    {
                        PlayerAnimator.SetTrigger(IdleTriggerName);
                        PlayerAnimator.ResetTrigger(MoveForwardTriggerName);
                        SetAllBoxColliders(false);
                    }
                }
                else
                {
                    // Debug.Log("Player is ATTACKING!");
                    SetAllBoxColliders(true);
                }
           // }
        }

        public void PlayAudio(int clip)
        {
            _audio.clip = AudiClips[clip];
            _audio.Play();
        }

        public void Punch()
        {
            IsAttacking = true;
            PlayerAnimator.ResetTrigger(IdleTriggerName);
            PlayerAnimator.SetTrigger(PunchTriggerName);
            PlayAudio(0);
        }

        public void Kick()
        {
            IsAttacking = true;
            PlayerAnimator.ResetTrigger(IdleTriggerName);
            PlayerAnimator.SetTrigger(KickTriggerName);
            PlayAudio(1);
        }

        public void React()
        {
            IsAttacking = false;
            Health = Health - _damage;
            PlayerHealthBar.value = Health;

            if (Health < 10)
            {
                PlayerDeath();
                PlayAudio(3);
            }
            else
            {
                PlayerAnimator.ResetTrigger(IdleTriggerName);
                PlayerAnimator.SetTrigger(ReactTriggerName);
                PlayAudio(2);
            }
        }

        public void PlayerDeath()
        {
            if(PlayerLifeCount <= 0)
            {
                GameController.Instance.SaveScore();
                StartCoroutine(GoToMainMenu());
                GameController.Instance.DoReset();

                return;
            }

            GameController.AllowedMovement = false;
            Health = 100;
            PlayerHealthBar.value = 100;
            PlayerAnimator.SetTrigger(DeathTriggerName);
            PlayerLifeCount--;
            GameController.Instance.OnScreenPoints(PlayerLifeCount);
            GameController.Instance.Rounds();

            StartCoroutine(ResetCharacters());
        }

        public void ResetToInitialPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(PlayerTagName);
            Transform t = clones[PlayerAvatarCloneIndex].GetComponent<Transform>();
            t.position = _initialPlayerPosition;
            transform.position = _initialPlayerPosition;
        }

        private void SetAllBoxColliders(bool state)
        {
            // Debug.Log("@@@ PLAYER Collider state " + state.ToString());
            foreach (BoxCollider boxCollider in Colliders)
            {
                boxCollider.enabled = state;
            }
        }

        private void ResetYPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(PlayerTagName);
            Transform t = clones[PlayerAvatarCloneIndex].GetComponent<Transform>();
            t.position.Set(newX: t.position.x, newY: 0.0f, newZ: t.position.z);
        }

        private float GetYPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(PlayerTagName);
            Transform t = clones[PlayerAvatarCloneIndex].GetComponent<Transform>();

            return t.position.y;
        }

        private IEnumerator ResetCharacters()
        {
            yield return new WaitForSeconds(4.0f);
            ResetToInitialPosition();
            PlayerAnimator.ResetTrigger(DeathTriggerName);
            PlayerAnimator.SetTrigger(IdleTriggerName);
            if (!GameController.Instance.MovementEnableOverride)
            {
                GameController.AllowedMovement = true;
            }
        }

        private IEnumerator GoToMainMenu()
        {
            yield return new WaitForSeconds(4.0f);

            // exit
        }
    }
}
