/*============================================================================== 
 Copyright (c) 2016-2017 PTC Inc. All Rights Reserved.
 
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved. 
 * ==============================================================================*/
using System.Collections;
using UnityEngine;
using Vuforia;

/// <summary>
/// This class implements the IVirtualButtonEventHandler interface and
/// contains the logic to start animations depending on what 
/// virtual button has been pressed.
/// </summary>
namespace Assets.Scripts
{
    public class PunchVirtualButtonEventHandler : MonoBehaviour,
                                             IVirtualButtonEventHandler
    {
        #region PUBLIC_MEMBERS
        public Material m_VirtualButtonDefault;
        public Material m_VirtualButtonPressed;
        public float m_ButtonReleaseTimeDelay;
        #endregion // PUBLIC_MEMBERS

        #region PRIVATE_MEMBERS
        VirtualButtonBehaviour[] virtualButtonBehaviours;
        #endregion // PRIVATE_MEMBERS

        #region MONOBEHAVIOUR_METHODS
        void Start()
        {
            // Register with the virtual buttons TrackableBehaviour
            virtualButtonBehaviours = GetComponentsInChildren<VirtualButtonBehaviour>();

            for (int i = 0; i < virtualButtonBehaviours.Length; ++i)
            {
                virtualButtonBehaviours[i].RegisterEventHandler(this);
            }
        }

        #endregion // MONOBEHAVIOUR_METHODS


        #region PUBLIC_METHODS
        /// <summary>
        /// Called when the virtual button has just been pressed:
        /// </summary>
        public void OnButtonPressed(VirtualButtonBehaviour vb)
        {
            Debug.Log("OnButtonPressed: " + vb.VirtualButtonName);

            SetVirtualButtonMaterial(m_VirtualButtonPressed);
            StartPlayerAction(vb.VirtualButtonName);

            StopAllCoroutines();

            BroadcastMessage("HandleVirtualButtonPressed", SendMessageOptions.DontRequireReceiver);
        }

        /// <summary>
        /// Called when the virtual button has just been released:
        /// </summary>
        public void OnButtonReleased(VirtualButtonBehaviour vb)
        {
            Debug.Log("OnButtonReleased: " + vb.VirtualButtonName);

            SetVirtualButtonMaterial(m_VirtualButtonDefault);
            EndPlayerAction(vb.VirtualButtonName);

            StartCoroutine(DelayOnButtonReleasedEvent(m_ButtonReleaseTimeDelay, vb.VirtualButtonName));
        }
        #endregion //PUBLIC_METHODS


        #region PRIVATE_METHODS
        private void StartPlayerAction(string buttonName)
        {
            switch(buttonName)
            {
                case "PunchVirtualButton":
                    FighterController.Instance.Punch();
                    return;
                case "KickVirtualButton":
                    FighterController.Instance.Kick();
                    return;
                case "MoveForwardVirtualButton":
                    FighterController.MoveForward = true;
                    return;
                case "MoveBackwardVirtualButton":
                    FighterController.MoveBack = true;
                    return;
                default:
                    FighterController.Instance.Punch();
                    return;
            }
        }

        private void EndPlayerAction(string buttonName)
        {
            switch (buttonName)
            {
                case "MoveForwardVirtualButton":
                    FighterController.MoveForward = false;
                    return;
                case "MoveBackwardVirtualButton":
                    FighterController.MoveBack = false;
                    return;
                default:
                    FighterController.MoveForward = false;
                    FighterController.MoveBack = false;
                    return;
            }
        }

        void SetVirtualButtonMaterial(Material material)
        {
            // Set the Virtual Button material
            for (int i = 0; i < virtualButtonBehaviours.Length; ++i)
            {
                if (material != null)
                {
                   // virtualButtonBehaviours[i].GetComponent<MeshRenderer>().sharedMaterial = material;
                }
            }
        }

        IEnumerator DelayOnButtonReleasedEvent(float waitTime, string buttonName)
        {
            yield return new WaitForSeconds(waitTime);

            BroadcastMessage("HandleVirtualButtonReleased", SendMessageOptions.DontRequireReceiver);
        }
        #endregion // PRIVATE METHODS
    }
}