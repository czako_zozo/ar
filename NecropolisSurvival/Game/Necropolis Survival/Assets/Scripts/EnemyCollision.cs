﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyCollision : MonoBehaviour
    {
        private const string PlayerTagName = "Player";

        private const string ReactTriggerName = "react";
        private const string PlayerFightIdleAnimatorName = "fight_idle";
        private const string PlayerKickAnimatorName = "roundhouse_kick 2";
        private const string PlayerPunchAnimatorName = "cross_punch";

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == PlayerTagName)
            {
                EnemyController.Instance.EnemyReact();
                Debug.Log("HIT");
            }
        }
    }
}
