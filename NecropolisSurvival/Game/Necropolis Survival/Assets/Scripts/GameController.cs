﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

namespace Assets.Scripts
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance;
        public static bool AllowedMovement = false;
        public static string HighScoreCacheName = "HighScores";
        public GameObject FlashButton;
        public GameObject CameraButton;
        public GameObject PlayerScoreOnScreen;
        public GameObject BackButton;
        public GameObject ForwardButton;
        public GameObject PunchButton;
        public GameObject KickButton;
        public GameObject StartStopButton;
        public GameObject ShowHideButton;
        public Text PlayerScoreTextField;
        public Text RoundCountTextField;
        public Text WaveCountTextField;
        public Text StartStopButtonText;
        public Text ShowHideButtonText;
        public AudioClip[] AudioClips;
        public GameObject[] points;
        private int[] _highScores;

        public static int PlayerScore = 0;
        public static int WaveNumber = 1;
        public static int EnemyCount = 1;
        public static int Round = 1;

        public bool MovementEnableOverride = true;

        private AudioSource _audioSource;
        private bool _played321 = false;
        private string MenuSceneName = "GameOver";

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        // Use this for initialization
        void Start ()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update () {
            if (!_played321)
            {
                if (UDTEventHandler.IsTracking)
                {
                    FlashButton.SetActive(false);
                    CameraButton.SetActive(false);
                    PlayerScoreOnScreen.SetActive(true);
                    BackButton.SetActive(true);
                    ForwardButton.SetActive(true);
                    PunchButton.SetActive(true);
                    KickButton.SetActive(true);
                    StartStopButton.SetActive(true);
                    ShowHideButton.SetActive(true);
                    FlashController.IsFlashOn = false;
                    _played321 = true;

                    StartCoroutine(Round1());
                }
            }
        }

        public void SetButtonsVisibility(bool visibility)
        {
            if (visibility)
            {
                ShowHideButtonText.text = "HIDE";
            }
            else
            {
                ShowHideButtonText.text = "SHOW";
            }
            BackButton.SetActive(visibility);
            ForwardButton.SetActive(visibility);
            PunchButton.SetActive(visibility);
            KickButton.SetActive(visibility);
            StartStopButton.SetActive(visibility);
        }

        public void EnableMovement()
        {
            AllowedMovement = !AllowedMovement;
            if(AllowedMovement)
            {
                StartStopButtonText.text = "STOP";
            }
            else
            {
                StartStopButtonText.text = "START";
            }
        }

        public void RestartGame()
        {
            GameController.AllowedMovement = false;
            DoReset();
            StartCoroutine(Round1());
            FighterController.Instance.ResetToInitialPosition();
            EnemyController.Instance.ResetToInitialPosition();
        }

        public void ScorePlayer()
        {
            PlayerScore += WaveNumber;
            PlayerScoreTextField.text = PlayerScore.ToString();
        }

        public void IncreaseWaveNumber()
        {
            WaveNumber++;
            WaveCountTextField.text = WaveNumber.ToString();
        }

        public void IncreaseEnemyCount()
        {
            EnemyCount += WaveNumber * 2;
        }

        public void Loose()
        {
            PlayAudioTrack(5);
            StartCoroutine(BackToGameMenu());
        }

        public void Win()
        {
            PlayAudioTrack(6);
            StartCoroutine(BackToGameMenu());
        }

        public static void PrintClones(string tag)
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(tag);
            Debug.Log(string.Format("---- START {0} POSITIONS ----", tag));
            Debug.Log(string.Format("*** COUNT: {0} ***", clones.Count()));
            foreach (var clone in clones)
            {
                Debug.Log(clone.ToString());
            }
            Debug.Log(string.Format("---- END {0} POSITIONS ----", tag));
        }

        public void OnScreenPoints(int lifeCount)
        {
            if (lifeCount > 2)
                return;

            points[lifeCount].SetActive(false);
        }

        public void Rounds()
        {
            Round = 3 - FighterController.Instance.PlayerLifeCount;
            RoundCountTextField.text = Round.ToString();

            if(Round == 1)
            {
                PlayAudioTrack(3);
            }

            if(Round == 2)
            {
                PlayAudioTrack(4);
            }
        }

        public void DoReset()
        {
            PlayerScore = 0;
            WaveNumber = 1;
            EnemyCount = 1;
            Round = 0;
            FighterController.Instance.Health = 100;
            EnemyController.Instance.EnemyHealth = 100;
            PlayerScoreTextField.text = "0";
            FighterController.Instance.PlayerHealthBar.value = 100;
            EnemyController.Instance.EnemyHealthBar.value = 100;
            FighterController.Instance.PlayerLifeCount = 3;
            foreach (var point in points)
            {
                point.SetActive(true);
            }
        }

        public void SaveScore()
        {
            List<int> newScores = new List<int>();

            newScores.AddRange(PlayerPrefsX.GetIntArray(HighScoreCacheName));
            newScores.Add(GameController.PlayerScore);

            PlayerPrefsX.SetIntArray(HighScoreCacheName, newScores.ToArray());
        }

        public void LoadMenu()
        {
            DoReset();
            SceneManager.LoadScene(MenuSceneName);
        }

        private void PlayAudioTrack(int clip)
        {
            _audioSource.clip = AudioClips[clip];
            _audioSource.Play();
        }

        IEnumerator BackToGameMenu()
        {
            yield return new WaitForSeconds(2.3f);

            LoadMenu();
        }

        IEnumerator Round1()
        {
            yield return new WaitForSeconds(0);

            PlayAudioTrack(0);
            StartCoroutine(PrepareYourself());
        }

        IEnumerator PrepareYourself()
        {
            yield return new WaitForSeconds(1.2f);

            PlayAudioTrack(1);
            StartCoroutine(Start321());
        }

        IEnumerator Start321()
        {
            yield return new WaitForSeconds(2.0f);

            PlayAudioTrack(2);
            StartCoroutine(AllowPlayerMovement());
        }

        IEnumerator AllowPlayerMovement()
        {
            yield return new WaitForSeconds(5.0f);
            if (!MovementEnableOverride)
            {
                AllowedMovement = true;
            }
        }
    }
}
