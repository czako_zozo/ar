﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerCollision : MonoBehaviour
    {
        private const string EnemyTagName = "Enemy";
        private const string EnemyAttackAnimatorName = "zombie_attack";

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == EnemyTagName)
            {
                FighterController.Instance.React();
                Debug.Log("ENEMY HIT");
            }
        }
    }
}
