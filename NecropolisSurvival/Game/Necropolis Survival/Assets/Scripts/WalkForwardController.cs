﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{
    public class WalkForwardController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

        public void OnPointerDown(PointerEventData eventData)
        {
            FighterController.MoveForward = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            FighterController.MoveForward = false;
        }
    }
}
