﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class ButtonsController : MonoBehaviour {

        public static bool IsVisible = true;

        public void ToggleButtonsVisibility()
        {
            IsVisible = !IsVisible;

            if (IsVisible)
            {
                GameController.Instance.SetButtonsVisibility(true);
            }
            else
            {
                GameController.Instance.SetButtonsVisibility(false);
            }
        }
    }
}
