﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{
    public class WalkBackController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
        public void OnPointerDown(PointerEventData eventData)
        {
            FighterController.MoveBack = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            FighterController.MoveBack = false;
        }
    }
}
