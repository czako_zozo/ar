﻿using UnityEngine;
using UnityEngine.XR;
using Vuforia;

public class ARCameraController : MonoBehaviour {

    public bool EnableVuforiaBehaviour = false;
    public bool EnableVideoBackgroundBehaviour = true;
    public bool EnableDefaultInitializationErrorHandler = true;

    // Use this for initialization
    void Start()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera)
        {
            if (mainCamera.GetComponent<VuforiaBehaviour>() != null)
            {
                if (EnableVuforiaBehaviour)
                {
                    VuforiaRuntime.Instance.InitVuforia();
                }
                else
                {
                    VuforiaRuntime.Instance.Deinit();
                }
                mainCamera.GetComponent<VuforiaBehaviour>().enabled = EnableVuforiaBehaviour;
                VuforiaBehaviour.Instance.enabled = EnableVuforiaBehaviour;
            }
            if (mainCamera.GetComponent<VideoBackgroundBehaviour>() != null)
            {
                mainCamera.GetComponent<VideoBackgroundBehaviour>().enabled = EnableVideoBackgroundBehaviour;
            }
            if (mainCamera.GetComponent<DefaultInitializationErrorHandler>() != null)
            {
                mainCamera.GetComponent<DefaultInitializationErrorHandler>().enabled = EnableDefaultInitializationErrorHandler;
            }

            mainCamera.clearFlags = CameraClearFlags.Skybox;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
