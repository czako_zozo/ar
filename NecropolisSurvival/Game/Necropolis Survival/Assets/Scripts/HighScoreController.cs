﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class HighScoreController : MonoBehaviour
    {
        private const string SceneName = "GameOver";
        private const string HighScoreCacheName = "HighScores";

        public Text HighScoreLabel;

        void Start()
        {
            var mockScores = new List<int> { 10, 15, 20, 25 };
            var descendingList = new List<int>();

            descendingList.AddRange(PlayerPrefsX.GetIntArray(HighScoreCacheName));
            descendingList.AddRange(mockScores);

            var index = descendingList.Count > 9 ? 10 : descendingList.Count;

            for (int i = 0; i < index; i++)
            {
                HighScoreLabel.text += i + " : " + descendingList[i] + "\n";
            }
        }

        void Update()
        {
            
        }

        public void BackButtonClicked()
        {
            SceneManager.LoadScene(SceneName);
        }
    }
}
