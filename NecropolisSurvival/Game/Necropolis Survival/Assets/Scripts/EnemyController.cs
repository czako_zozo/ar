﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class EnemyController : MonoBehaviour
    {
        public Transform PlayerTransform;
        public BoxCollider[] Colliders;
        public AudioClip[] AudiClips;
        public Slider EnemyHealthBar;

        public static Animator EnemyAnimator;
        public static EnemyController Instance;

        public int EnemyHealth = 100;

        private AudioSource _audioEnemy;
        private Vector3 _direction;
        private int _damage = 10;
        private Vector3 _initialEnemyPosition;

        private const string MoveBackTriggerName = "back";
        private const string MoveForwareTriggerName = "forward";
        private const string IdleTriggerName = "idle";
        private const string PunchTriggerName = "punch";
        private const string ReactTriggerName = "enemy_react";
        private const string DeathTriggerName = "dying";
        private const string WalkTriggerName = "walk";
        private const string WalkBackTriggerName = "walkBack";

        private const string FightIdleAnimatorStateName = "fight_idle";
        private const string ZombieAttackAnimatorStateName = "zombie_attack";
        private const string ZombieReactAnimatorStateName = "Zombie_Reaction_Hit";

        private const string EnemyTagName = "EnemyAvatar";

        private const float RotationSpeed = 0.5f;
        private const float PunchDistance = 2.9f;
        private const float MinMoveBackDistance = 0f;
        private const float MaxMoveBackDistance = 2.5f;

        private const int EnemyAvatarCloneIndex = 0;

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        // Use this for initialization
        void Start ()
        {
            EnemyAnimator = GetComponent<Animator>();
            _audioEnemy = GetComponent<AudioSource>();
            SetAllBoxColliders(false);
            _initialEnemyPosition = transform.position;
            Debug.Log("Enemy initial position (x, y, z): (" +
                _initialEnemyPosition.x + ", " +
                _initialEnemyPosition.y + ", " +
                _initialEnemyPosition.z + ")");
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (UDTEventHandler.IsTracking && GetYPosition() > 0)
            {
                ResetYPosition();
            }

            if (EnemyAnimator.GetCurrentAnimatorStateInfo(0).IsName(FightIdleAnimatorStateName))
            {
                _direction = PlayerTransform.position - this.transform.position;
                _direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(
                    this.transform.rotation, Quaternion.LookRotation(_direction),
                    RotationSpeed);
                SetAllBoxColliders(false);
            }

            // Debug.Log("--- MAGNITUDE: " + _direction.magnitude);

            if (_direction.magnitude > PunchDistance && GameController.AllowedMovement)
            {
                EnemyAnimator.SetTrigger(WalkTriggerName);
                _audioEnemy.Stop();
                SetAllBoxColliders(false);
            }
            else
            {
                //EnemyAnimator.ResetTrigger(WalkTriggerName);
            }

            if (_direction.magnitude <= PunchDistance && MaxMoveBackDistance > _direction.magnitude && GameController.AllowedMovement)
            {
                // Debug.Log("Player is ATTACKING!");
                SetAllBoxColliders(true);
                if (!_audioEnemy.isPlaying && !EnemyAnimator.GetCurrentAnimatorStateInfo(0).IsName(ZombieAttackAnimatorStateName))
                {
                    PlayAudio(0);
                    EnemyAnimator.SetTrigger(PunchTriggerName);
                }
            }
            else
            {
                //EnemyAnimator.ResetTrigger(PunchTriggerName);
            }

            if (_direction.magnitude <= MaxMoveBackDistance && GameController.AllowedMovement)
            {
                EnemyAnimator.SetTrigger(WalkBackTriggerName);
            }
            else
            {
                //EnemyAnimator.ResetTrigger(WalkBackTriggerName);
            }
        }

        public void PlayAudio(int clip)
        {
            if (_audioEnemy.isPlaying && EnemyAnimator.GetCurrentAnimatorStateInfo(0).IsName(ZombieReactAnimatorStateName))
            {
                _audioEnemy.Stop();
            }
            _audioEnemy.clip = AudiClips[clip];
            _audioEnemy.Play();
        }

        public void EnemyReact()
        {
            EnemyHealth = EnemyHealth - _damage;
            EnemyHealthBar.value = EnemyHealth;

            if (EnemyHealth < 10)
            {
                _audioEnemy.Stop();
                EnemyAnimator.ResetTrigger(ReactTriggerName);
                EnemyDeath();
            }
            else
            {
                _audioEnemy.Stop();
                EnemyAnimator.SetTrigger(ReactTriggerName);
                PlayAudio(2);
            }
        }

        public void EnemyDeath()
        {
            GameController.AllowedMovement = false;
            EnemyHealth = 100;
            EnemyHealthBar.value = 100;
            Debug.Log("ENEMY DEATH");
            _audioEnemy.Stop();
            EnemyAnimator.SetTrigger(DeathTriggerName);
            GameController.Instance.ScorePlayer();
            GameController.Instance.IncreaseWaveNumber();

            StartCoroutine(ResetCharacters());
        }

        public void ResetToInitialPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(EnemyTagName);
            Transform t = clones[EnemyAvatarCloneIndex].GetComponent<Transform>();
            t.position.Set(newX: _initialEnemyPosition.x, newY: 0.0f, newZ: _initialEnemyPosition.z);
            transform.position = _initialEnemyPosition;
        }

        private void SetAllBoxColliders(bool state)
        {
            // Debug.Log("@@@ ENEMY Collider state " + state.ToString());
            foreach (BoxCollider boxCollider in Colliders)
            {
                boxCollider.enabled = state;
            }
        }

        private void ResetYPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(EnemyTagName);
            Transform t = clones[EnemyAvatarCloneIndex].GetComponent<Transform>();
            t.position.Set(newX: t.position.x, newY: 0.0f, newZ: t.position.z);
        }

        private float GetYPosition()
        {
            GameObject[] clones = GameObject.FindGameObjectsWithTag(EnemyTagName);
            Transform t = clones[EnemyAvatarCloneIndex].GetComponent<Transform>();

            return t.position.y;
        }

        private IEnumerator DestroyEnemyObject()
        {
            _audioEnemy.Stop();
            yield return new WaitForSeconds(5.0f);

            // destory enemy object
        }

        private IEnumerator ResetCharacters()
        {
            yield return new WaitForSeconds(4.0f);
            ResetToInitialPosition();
            EnemyAnimator.ResetTrigger(DeathTriggerName);
            EnemyAnimator.SetTrigger(IdleTriggerName);
            if (!GameController.Instance.MovementEnableOverride)
            {
                GameController.AllowedMovement = true;
            }
        }
    }
}
